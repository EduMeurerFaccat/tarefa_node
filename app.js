var app = require("http");

const hostame = "localhost";
const port = 4000;

const server = app.createServer((req, res)=>{
    res.statusCode = 200;
    res.setHeader("Content-Type", "text/plain");
    res.end("Hello world");
})

server.listen(port,hostame, ()=>{
    console.log(`Rodando no endereço: http://${hostame}:${port}`);
})