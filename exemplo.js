var express = require('express');
var app = express();

app.get("/", (req, res)=>{
    res.send("<a href='http://localhost:3000/teste'>Teste</a>");
})

function teste(obj){
    
    console.log(this.this);
    console.log(obj);
}

app.get("/teste", (req, res)=>{
    
    var a = "teste";
    teste();
    teste.call(
        {this: "Teste com call"}
    );
    teste.apply(
        {this: "bar"},
        ["bas"]
    );
    res.send("Teste de rota");
})

app.listen(3000, ()=>{
    console.log("Rodando no endereço: http://localhost:3000");
});
